#pragma once
#include "SDL_events.h"
#include "Keys.h"
/*Interfame to SDL2 engine*/
namespace SDL2 {
	//Space with event classes and flags
	namespace Events {
		class Event {
		public:
			/*Key getter
			*@return Uint32 number of pressed key*/
			Uint32 inline key()const {
				return _event.key.keysym.sym;
			}

			/*Poll event
			*@return true if any pending events or false if not*/
			bool inline poll() {
				return SDL_PollEvent(&_event);
			}

			/*Type getter
			*@return Uint32 number of used type*/
			Uint32 inline type() {
				return _event.type;
			}

			/*Check if key is pressing
			*@return bool true if pressed or false if not*/
			bool inline keyDown()const {
				return (_event.type == SDL_KEYDOWN)?true:false;
			}


			/*Check if mouse buttor is pressing
			*@return bool true if pressed or false if not*/
			bool inline buttonDown()const {
				return (_event.type == SDL_MOUSEBUTTONDOWN)?true:false;
			}

			/*SDL_Event getter
			*@return SDL_Event reference*/
			inline SDL_Event& get() {
				return _event;
			}
		private:
			SDL_Event _event;
		};
	}
}