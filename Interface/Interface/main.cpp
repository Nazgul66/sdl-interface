#include <cstdlib>
#include "Init.h"
#include "Text.h"
#include "Renderer.h"
#include "Texture.h"
#include "ManagerRenderer.h"
#define SDL_MAIN_HANDLED
#undef main

bool init() {
	if(!SDL2::init(SDL_INIT_EVERYTHING))
		return false;
	SDL2::initSubSystem(SDL_INIT_EVERYTHING);

	if(TTF_Init())
		return false;
	return true;
}

void quit() {
	SDL2::quitSubSystem(SDL_INIT_EVERYTHING);
	SDL2::quit();
	TTF_Quit();
}

void loop() {
	SDL2::Display::Window w("My window", 800, 600);
	SDL2::Events::Event e;
	SDL2::Display::Renderer r(w);
	SDL2::Graphic::Source s("../images/images_15.bmp", r.get());
//	SDL2::Graphic::Sprite sprite(s);
//	sprite.rectShowed = sprite.rectSource = {0, 0, 800, 600};

	SDL_SetRenderDrawColor(r.get(), 0, 0, 255, 255);
	do {
		r.clear();
		e.poll();
	//	r.copy(s.texture(), &sprite.rectSource, &sprite.rectShowed);
		r.present();
	} while(e.type()!=SDL_QUIT && e.key() != SDL2::Events::Key::escape);
}

int main() {
	init();
	loop();
	quit();
	return EXIT_SUCCESS;
}
